package com.tsystems.javaschool.tasks.zones;

import java.util.*;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){

        HashMap<Integer, Zone> zoneList = getZoneList(zoneState);

        for (int k=0; k< requestedZoneIds.size();k++){
            if (!zoneList.containsKey(requestedZoneIds.get(k))) {
                return false;
            }
        }

        List<Integer> remainingZones = new ArrayList<Integer>(requestedZoneIds);
        List<Integer> connectedZones = new ArrayList<Integer>(Arrays.asList(remainingZones.get(0)));
        remainingZones.remove(0);
        List<Integer> collectiveNeighbours = new ArrayList<>();
        collectiveNeighbours.addAll(zoneList.get(connectedZones.get(0)).getNeighbours());
        collectiveNeighbours.add(connectedZones.get(0));
        int i = 0;

        boolean isChanged = false;
        while (!remainingZones.isEmpty()) {

            Integer checkingZone = remainingZones.get(i);

            for (int j = 0; j < connectedZones.size(); j++) {
                if (zoneList.get(checkingZone).getNeighbours().contains(connectedZones.get(j)) || collectiveNeighbours.contains(checkingZone)) {
                    connectedZones.add(checkingZone);
                    collectiveNeighbours.addAll(zoneList.get(checkingZone).getNeighbours());
                    collectiveNeighbours.add(checkingZone);
                    isChanged = true;
                    break;
                }
            }
            if (isChanged == true) {
                remainingZones.remove(i);
                isChanged = false;
                i=0;
            } else {
                if (i + 1 == remainingZones.size()) {
                    break;
                }
                i++;
            }
        }

        Collections.sort(connectedZones);
        Collections.sort(requestedZoneIds);

        return connectedZones.equals(requestedZoneIds);
    }

    private HashMap<Integer, Zone> getZoneList(List<Zone> zoneState) {
        HashMap<Integer, Zone> zoneList = new HashMap<Integer, Zone>();
        for (Zone zone : zoneState) {
            zoneList.put(zone.getId(), zone);
        }
        return zoneList;
    }
}
