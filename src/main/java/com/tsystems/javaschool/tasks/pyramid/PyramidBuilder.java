package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int height = getPyramidHeight(inputNumbers.size());
        int width = height + height - 1;
        int[][] Pyramid = new int[height][width];

        try {
            Collections.sort(inputNumbers);
            int currentPosition = 0;
            for (int i = 0; i < height; i++) {
                for (int j = height - 1 - i; j < height + i; j = j + 2) {
                    Pyramid[i][j] = inputNumbers.get(currentPosition++);
                }
            }
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
        return Pyramid;
    }

    public int getPyramidHeight(int Size) {
        double d = 1 + 8 * Size;
        double height = (-1 + Math.sqrt(d)) / 2;
        if (height != Math.floor(height)) {
            throw new CannotBuildPyramidException();
        }
        return (int) height;
    }
}
